# Detalles del Festival Latinoamericano de Instalación de Software Libre (FLISol) Tenerife 2021

## 1. Ponentes

* Juan Julián Merelo
* Luis Fajardo López
* Juan A. Hernández Cabrera
* Fernando A. Muñoz Villarejo
* Brais Arias Río
* Sofía Prósper Díaz-Mor
* Santiago Saavedra López
* Marelisa Blanco
* Iván Rodríguez Méndez
* Iván Hernández Cazorla
* Almudena García
* Helena Amaral
* María Arias de Reyna
* Luis Falcón
* Jesús M. González-Barahona

## 2. Duración de la actividad

5 horas lectivas

## 3. Fecha de las sesiones virtuales

Sábado 24 de abril, de 9:00 a 14:00 hora canaria (GMT+1)

## 4. Lugar de celebración

En línea, entrando registrado en la plataforma BigBlueButton del proyecto
[EDUCATIC](https://bbb.educar.encanarias.info), las salas concretas se 
enviarán por correo a los registrados.

## 5. Número de plazas 

300

## 6. Destinado a

Cualquier persona

## 7. Proceso de inscripción y listas de admitidos

A través de la Fundación General de la Universidad de La Laguna

* Plazo de inscripción: del lunes 19 de abril de 2021 al viernes 23 a las 12 horas

## 8. Objetivos

El FLISoL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público. Tenerife será una de las sedes (#FLISoLTenerife2021), colaborando con la organización la Oficina de Software Libre de la Universidad de La Laguna, y realizándose en línea debido a las restricciones impuestas por la pandemia. La asistencia es libre y gratuita, su principal objetivo es promover el uso del software libre, dando a conocer al público en general su filosofía, alcance, avances y desarrollo. Para ello se organizarán charlas de diversas temáticas a lo largo de toda la mañana.

## 9. Comptencias

## 10. Contenidos

Consultar el [Programa](programa.md)

## 11. Metodología

Se realizarán charlas expositivas de cada ponencia, teniendo la oportunidad de un turno de preguntas al final de las mismas. Se habilitarán dos salas diferentes, excepto para la mesa redonda final, con diferente programa de charlas.

## 12. Evaluación

No se realizará evaluación, pero se controlará la asistencia para la emisión de certificados. Cada asistente deberá estar conectado mediante una cuenta registrada en alguna de las dos salas donde se realizan las charlas, pudiendo cambiar entre salas según sus preferencias.

## 13. Certificado

Se emitirá certificado de asistencia

## 14. Información general

Actividad co-organizada por la Oficina de Software Libre de la Universidad de La Laguna. Más información en la (web oficial de FLISoL)[https://flisol.info/FLISOL2021/Espana/Tenerife]

## 15. Material necesario

Dispositivo (preferentemente PC o portatil) con navegador web (moderno, p.e.: Firefox, Chromium, Vivaldi, Chrome, ...), altavoces (o auriculares), micrófono y conexión a internet.
