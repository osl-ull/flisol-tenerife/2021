# FLISOL TENERIFE 2021

## Repositorio del Programa del FLISoL-Tenerife 2021 y sus recursos

Este repositorio alberga la información del programa y los detalles del evento. 

* [Programa](programa.md)
* [Detalles del evento](detalles.md)


## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2021/Espana/Tenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>
+ [Correo](mailto://flisoltenerife@disroot.org): flisoltenerife@disroot.org
+ Redes Sociales: 
	+ [Mastodon](https://txs.es/@flisoltenerife): [@flisoltenerife@txs.es](https://txs.es/@flisoltenerife)
	+ [Twitter](https://twitter.com/flisoltenerife): https://twitter.com/flisoltenerife
	
	
