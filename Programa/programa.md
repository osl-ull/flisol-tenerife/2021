# Programa del Festival Latinoamericano de Instalación de Software Libre (FLISol) Tenerife 2021

**Fecha y hora:** Sábado, 24 de Abril de 2021, de 9:00 a 14:00 hora canaria (GMT+1)

## Sala 1 Filosofía de Software Libre

### 9:00 Presentación del evento

**Presenta:** *Luis Fajardo*

+ 09:20 - 10:00, **J.J. Merelo**, *Cómo defender el software libre (y cómo no hacerlo)*
+ 10:00 - 10:20, **Marelisa Blanco**, *El apasionante mundo de las licencias del software*
+ 10:20 - 11:00, **Jesús M. Gonzalez-Barahona**. *¿Qué es el Software Libre?*

### 11:00 - 11:20,  Descanso (Barraquito time)

**Presenta:** *Marelisa Blanco*

+ 11:20 - 12:00, **Sofía Prósper Díaz-Mor** y **Santiago Saavedra López**, *Claro que tienes algo que esconder, y eso está bien*
+ 12:00 - 12:20, **Luis Fajardo López**, *TXS.es y el Fediverso: sol entre las nubes grises*
+ 12:20 - 13:00, **Luis Falcón**, *El proyecto GNU Health como modelo de Software Libre en el sistema de Salud Pública*

### 13:00 - 14:00, Mesa redonda *Cultura libre y tecnología para la sociedad*, modera **Luis Fajardo López**

## Sala 2 Aplicaciones y Talleres de Software Libre

**Presenta:** *Juan Febles*

+ 09:20 - 10:00, **Brais Arias Río**, *Servicios y aplicaciones alternativos y libres*
+ 10:00 - 10:20, **Iván Rodríguez Méndez**, *KiCad: Diseño electrónico a medida*
+ 10:20 - 10:40, **María Arias de Reyna**, *Ensillando Apache Camel para integrar componentes*
+ 10:40 - 11:00: **Fernando A. Muñoz Villarejo**, *Instalación y primeros pasos con Zotero, gestor de referencias bibliográficas*

### 11:00 - 11:20,  Descanso (Barraquito time)

**Presenta:** *Fernando Rosa*

+ 11:20 - 12:00, **Juan A. Hernández Cabrera**, *ULLRToolbox: Análisis estadísticos simples y complejos en R sin saber R* 
+ 12:00 - 12:20, **Almudena García**, *Juegos didácticos con tipos de datos en C*
+ 12:20 - 12:40, **Helena Amaral**, *Una mirada a Wikipedia*
+ 12:40 - 13:00, **Iván Hernández Cazorla**, *Como consultar datos en Wikidata*

### 13:00 - 14:00, Mesa redonda *Cultura libre y tecnología para la sociedad*, modera **Luis Fajardo López**
