
# PRESENTACIONES FLISOL TENERIFE 2021
Esta carpeta alberga todas las presentaciones del FLISoL Tenerife 2021

La organización está trabajando para estructurar y compartir todos los documentos en este repositorio gitlab.

## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2021/Espana/Tenerife>
+ Correo: [flisoltenerife@disroot.org](mailto:flisoltenerife@disroot.org)
+ Mastodon: <https://txs.es/@flisoltenerife>
+ Peertube: <https://tuvideo.encanarias.info/accounts/flisoltenerife>
+ Archive.org: <https://archive.org/details/@flisoltenerife>
+ Twitter: <https://twitter.com/flisoltenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>

